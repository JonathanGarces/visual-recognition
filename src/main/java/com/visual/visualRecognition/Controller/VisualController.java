package com.visual.visualRecognition.Controller;

import java.io.File;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ibm.watson.visual_recognition.v4.model.AnalyzeResponse;
import com.visual.visualRecognition.Entity.Authenticator;
import com.visual.visualRecognition.Entity.Clasifier;
import com.visual.visualRecognition.Entity.Image;
import com.visual.visualRecognition.Entity.Person;
import com.visual.visualRecognition.Entity.Response;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableCircuitBreaker
@EnableEurekaClient
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("/visual")
public class VisualController {

	@Autowired
	Authenticator authenticator;

	void saveLog(String message, String status, String service) {
		RestTemplate template = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("message", message);
		map.add("status", status);
		map.add("service", service);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		String logService = "https://coe-logging.herokuapp.com/logTransaction";
		String result = template.postForObject(logService, request, String.class);

	}
    
	@HystrixCommand(fallbackMethod = "verifyError")
	@PostMapping("/verify")
	public HashMap verifyP(@RequestBody HashMap data) {

		String image = (String) data.get("image");
		String base64Image = image.split(",")[1];
		byte[] d = Base64.getMimeDecoder().decode(base64Image);
		long now = System.currentTimeMillis();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").format(new Date());
		String imageName = "user_image" + now + ".jpg";
		String fileURL = "./user_images/" + imageName;
		HashMap res = null;
		
		try (OutputStream stream = new FileOutputStream(fileURL)) {
			stream.write(d);
			res = authenticator.VerifyP(imageName, fileURL);
			System.out.println(res);
			//return res;
			/*if (res != null) {
				String message = (String) res.get("message");
				String status = (String) res.get("status");
				String service = (String) res.get("service");
				saveLog(message, status, service);
			}*/
			
			
			//return res;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return res;
	}
	
	public HashMap verifyError(@RequestBody HashMap data) {
		HashMap res = new HashMap();
		res.put("error", "It is not possible verify the person");
		return res;
		
	}
	
	@HystrixCommand(fallbackMethod = "errorZulu")
	@PostMapping("/testZulu")
	public HashMap test(@RequestBody HashMap data) {
		
		
		String image = (String) data.get("image");
		
		String base64Image = image.split(",")[1];
		byte[] d = Base64.getMimeDecoder().decode(base64Image);
		long now = System.currentTimeMillis();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").format(new Date());
		String imageName = "user_image" + now + ".jpg";
		String fileURL = "./user_images/" + imageName;
		HashMap res = null;
		
		
		try (OutputStream stream = new FileOutputStream(fileURL)) {
			stream.write(d);
			//res = authenticator.VerifyP(imageName, fileURL);
			System.out.println(res);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//return data;

		if (res != null) {
			String message = (String) res.get("message");
			String status = (String) res.get("status");
			String service = (String) res.get("service");
			saveLog(message, status, service);
		}

		//return res;
		return data;
	}
	
	public HashMap errorZulu(@RequestBody HashMap data) {
		HashMap res = new HashMap();
		res.put("error", "Error");
		return res;
		
	}
	
	
    @HystrixCommand(fallbackMethod = "metodoAlternativo")
	@GetMapping("/testZulu1")
	public String testZulu() {
		
		return "get test zulu from local";
	}
	
	public String metodoAlternativo() {
		
		return "alternative";
	}

}
