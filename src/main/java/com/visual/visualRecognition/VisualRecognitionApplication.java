package com.visual.visualRecognition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;


import com.visual.visualRecognition.Entity.Authenticator;

@EnableEurekaClient
@SpringBootApplication
public class VisualRecognitionApplication {
	
	
	@Bean
	public Authenticator getAuthenticator() {return new Authenticator();}

	public static void main(String[] args) {
		SpringApplication.run(VisualRecognitionApplication.class, args);
	}

}
