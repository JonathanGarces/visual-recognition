package com.visual.visualRecognition.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ibm.watson.visual_recognition.v4.model.AnalyzeResponse;
import com.visual.visualRecognition.Entity.Authenticator;
import com.visual.visualRecognition.Entity.Clasifier;
import com.visual.visualRecognition.Entity.Image;
import com.visual.visualRecognition.Entity.Person;
import com.visual.visualRecognition.Entity.Response;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("/visual")
public class VisualController {

	@Autowired
	Authenticator authenticator;

	// @CrossOrigin(origins = "http://localhost:8080")
	@PostMapping("/verifyPerson")
	public HashMap verify(@RequestBody HashMap data) {

		HashMap r = new HashMap();

		String image = (String) data.get("image");
		String base64Image = image.split(",")[1];
		byte[] d = Base64.getMimeDecoder().decode(base64Image);

		long now = System.currentTimeMillis();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").format(new Date());

		String imageName = "user_image" + now + ".jpg";
		String fileURL = "./user_images/" + imageName;
		Response response = null;
		// String path = "C:\\Users\\Ene\\Desktop\\test_image.jpg";
		try (OutputStream stream = new FileOutputStream(fileURL)) {
			stream.write(d);

			response = authenticator.VerifyPerson(imageName, fileURL);

			List<Person> persons = new ArrayList();

			for (Image img : response.getImages()) {
				for (Clasifier clasifier : img.getClasifiers()) {
					List<Person> p = clasifier.getPersons().stream().filter(x -> x.getScore() > 0.50)
							.collect(Collectors.toList());
					persons.addAll(p);
				}
			}

			System.out.println("Persons");
			System.out.println(persons.size());

			String message = "Error, user is not registered";
			String status = "401 Request had invalid authentication credentials.";
			boolean res = false;
			if (persons.size() > 0) {
				Person person = Collections.max(persons, Comparator.comparing(s -> s.getScore()));
				if (person.getType().equals("Person")) {
					r.put("message", "Error, user is not registered");
					r.put("result", res);
				} else {
					res = true;
					message = "Success,Welcome " + person.getType();
					r.put("name", person.getType());
					r.put("message", "Welcome " + person.getType());
					r.put("result", res);
					status = "200 OK";
					System.out.println(person);
				}

			} else {
				r.put("message", "Error, user is not registered");
				r.put("result", res);
			}
			// saveLog(message,status,Boolean.toString(res));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return r;
		// return authenticator.VerifyPerson();
	}

	void saveLog(String message, String status, String service) {
		RestTemplate template = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("message", message);
		map.add("status", status);
		map.add("service", service);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		String logService = "https://coe-logging.herokuapp.com/logTransaction";
		String result = template.postForObject(logService, request, String.class);
		System.out.print("save transaction: " + result);

	}

	@PostMapping("/verify")
	public HashMap verifyP(@RequestBody HashMap data) {

		String image = (String) data.get("image");
		String base64Image = image.split(",")[1];
		byte[] d = Base64.getMimeDecoder().decode(base64Image);

		long now = System.currentTimeMillis();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").format(new Date());

		String imageName = "user_image" + now + ".jpg";
		String fileURL = "./user_images/" + imageName;
		HashMap res = null;
		try (OutputStream stream = new FileOutputStream(fileURL)) {
			stream.write(d);
			res = authenticator.VerifyP(imageName, fileURL);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (res != null) {
			String message = (String) res.get("message");
			String status = (String) res.get("status");
			String service = (String) res.get("service");
			saveLog(message, status, service);
		}

		return res;
	}

}
