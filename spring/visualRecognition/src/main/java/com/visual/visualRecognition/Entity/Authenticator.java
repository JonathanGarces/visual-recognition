package com.visual.visualRecognition.Entity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.cloud.sdk.core.service.model.FileWithMetadata;
import com.ibm.cloud.sdk.core.service.model.FileWithMetadata.Builder;
import com.ibm.watson.developer_cloud.service.security.IamOptions;

import com.ibm.watson.visual_recognition.v4.VisualRecognition;
//import com.ibm.watson.visual_recognition.v4.VisualRecognition;


import com.ibm.watson.visual_recognition.v4.model.AnalyzeOptions;
import com.ibm.watson.visual_recognition.v4.model.AnalyzeResponse;
import com.ibm.watson.visual_recognition.v4.model.CollectionObjects;
import com.ibm.watson.visual_recognition.v4.model.DetectedObjects;
import com.ibm.watson.visual_recognition.v4.model.ObjectDetail;


public class Authenticator {
	private final String APIKey = "nSIr-iKhw2uTUP8BW2MYYTBHR4S9roGSkXUd2YUVpGc3";
	private Response response;
	
	public Response VerifyPerson(String image, String url) {
		
		IamOptions options = new IamOptions.Builder()
				.apiKey(APIKey)
				.build();

			/*VisualRecognition service = new VisualRecognition("2018-03-19", options);

			ClassifiedImages result = null;
			InputStream imagesStream;
			try {
				imagesStream = new FileInputStream(url);
				ClassifyOptions classifyOptions = new ClassifyOptions.Builder()
						.imagesFile(imagesStream)
						.imagesFilename(image)
						.threshold((float) 0.6)//DefaultCustomModel_1168983804
						.classifierIds(Arrays.asList("DefaultCustomModel_1168983804"))
						.build();

					 result = service.classify(classifyOptions).execute();
					System.out.println(result);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
		
		if(result != null)
			this.response = new Response(result);
		else
			this.response = new Response();
		*/
		return response;
	}
	
	public HashMap VerifyP(String image, String url) {
		IamOptions options1 = new IamOptions.Builder()
				.apiKey(APIKey)
				.build();
		
		IamAuthenticator authenticator = new IamAuthenticator(APIKey);
		VisualRecognition visualRecognition = new VisualRecognition("2018-03-19",authenticator);
		HashMap json = new HashMap();
		
		
		
		FileWithMetadata diceImage = null;
		try {
			diceImage = new FileWithMetadata.Builder()
					  .data(new File(url))
					  .contentType("image/jpeg")
					  .build();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				  
				List<FileWithMetadata> filesToAnalyze = Arrays.asList(diceImage);
				
		AnalyzeOptions options = new AnalyzeOptions.Builder()
				  .imagesFile(filesToAnalyze)
				  .addCollectionIds("af519149-c1f3-4fff-b9fe-df290e56de89")
				  .addFeatures("objects")
				  .build();

		       AnalyzeResponse response = visualRecognition.analyze(options).execute().getResult();
		       
		       System.out.println(response);
		       List<ObjectDetail> persons =  new ArrayList();
		        List<com.ibm.watson.visual_recognition.v4.model.Image> images = response.getImages();
		        for(com.ibm.watson.visual_recognition.v4.model.Image image1 : images ) {
		        	 DetectedObjects objects = image1.getObjects();
		        	  if(objects != null) {
		        		  List<CollectionObjects> collections = objects.getCollections();
		        		  
		        		  if(collections != null) {
		        			  for(CollectionObjects col : collections) {
				        		  List<ObjectDetail> p = col.getObjects().stream().filter(x -> x.getScore() > 0.1)
								          .collect(Collectors.toList());
				        		  persons.addAll(p);
				        		  System.out.println(p);
				        	 }
		        		  }
		        		  
				        	
		        	  }
		        	
		        	
		        }
		       
				
		        if(persons.size() > 0) {
					 ObjectDetail person = Collections.max(persons, Comparator.comparing(s -> s.getScore()));
					json.put("name", person.getObject());
					json.put("score", person.getScore());
					json.put("message", "Welcome " + person.getObject());
					json.put("status", "200 OK");
					json.put("service", "Login");
					json.put("result", true);
					 
		        }else {
		        	json.put("messgae", "Error, user is not registered");
		        	json.put("status", "401 Request had invalid authentication credentials.");
					json.put("service", "Login");
					json.put("result", false);
		        }
			

			return json;
			 
	}

}
