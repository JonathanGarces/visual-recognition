package com.visual.visualRecognition.Entity;

import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassResult;

public class Person {

	private String type;
	private Float score;
	private String type_hierarchy;
	
	Person(){
		
	}
	
	Person(ClassResult result){
		this.type = result.getClassName();
		this.score = result.getScore();
		this.type_hierarchy = result.getTypeHierarchy();
		
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Float getScore() {
		return score;
	}
	public void setScore(Float score) {
		this.score = score;
	}
	public String getType_hierarchy() {
		return type_hierarchy;
	}
	public void setType_hierarchy(String type_hierarchy) {
		this.type_hierarchy = type_hierarchy;
	}

	@Override
	public String toString() {
		return "Person [type=" + type + ", score=" + score + ", type_hierarchy=" + type_hierarchy + "]";
	}
	
	
	
}
