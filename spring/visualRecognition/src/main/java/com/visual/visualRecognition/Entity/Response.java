package com.visual.visualRecognition.Entity;

import java.util.List;
import java.util.stream.Collectors;

import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifiedImage;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifiedImages;

public class Response {

	private Long images_processed;
	private Long custom_classes;
	private List<Image> images;

	public Response() {

	}

	public Response(ClassifiedImages result) {
		
		this.custom_classes = result.getCustomClasses();
		this.images_processed = result.getImagesProcessed();
		List<ClassifiedImage> list = result.getImages();
		this.images = list.stream().map(Image::new).collect(Collectors.toList());
		
		
	}

	public Long getImages_processed() {
		return images_processed;
	}

	public void setImages_processed(Long images_processed) {
		this.images_processed = images_processed;
	}

	public Long getCustom_classes() {
		return custom_classes;
	}

	public void setCustom_classes(Long custom_classes) {
		this.custom_classes = custom_classes;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

}
