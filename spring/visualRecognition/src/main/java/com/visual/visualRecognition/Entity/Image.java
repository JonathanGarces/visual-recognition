package com.visual.visualRecognition.Entity;

import java.util.List;
import java.util.stream.Collectors;

import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifiedImage;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifierResult;

public class Image {

	private String source_url;
	private String resolved_url;
	private String image;
	private List<Clasifier> clasifiers;
	
	
	public Image() {}
	
	public Image(ClassifiedImage image) {
		
		this.resolved_url = image.getResolvedUrl();
		this.source_url = image.getSourceUrl();
		this.image = image.getImage();
		List<ClassifierResult>list = image.getClassifiers();
		this.clasifiers = list.stream().map(Clasifier::new).collect(Collectors.toList());
	}
	
	public String getSource_url() {
		return source_url;
	}
	public void setSource_url(String source_url) {
		this.source_url = source_url;
	}
	public String getResolved_url() {
		return resolved_url;
	}
	public void setResolved_url(String resolved_url) {
		this.resolved_url = resolved_url;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public List<Clasifier> getClasifiers() {
		return clasifiers;
	}
	public void setClasifiers(List<Clasifier> clasifiers) {
		this.clasifiers = clasifiers;
	}
	
	
}
