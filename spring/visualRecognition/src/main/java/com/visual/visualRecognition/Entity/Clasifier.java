package com.visual.visualRecognition.Entity;

import java.util.List;
import java.util.stream.Collectors;

import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassResult;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifierResult;

public class Clasifier {

	private String classifier_id;
	private String name;
	private List<Person> persons;
	
	public Clasifier() {}
	
	public Clasifier(ClassifierResult clasifier) {
		
		this.classifier_id = clasifier.getClassifierId();
		this.name = clasifier.getName();
		List<ClassResult> list = clasifier.getClasses();
		this.persons = list.stream().map(Person::new).collect(Collectors.toList());
		
	}
	public String getClassifier_id() {
		return classifier_id;
	}
	public void setClassifier_id(String classifier_id) {
		this.classifier_id = classifier_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Person> getPersons() {
		return persons;
	}
	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}
	
	
}
